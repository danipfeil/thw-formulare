import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import {MatInputModule} from '@angular/material/input';
import { RouterModule, Routes } from '@angular/router';
import { MarchCalculatorComponent } from './march-calculator/march-calculator.component';
import { ReactiveFormsModule } from '@angular/forms';
import { MarchCalculatorLengthComponent } from './march-calculator/march-calculator-length/march-calculator-length.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatButtonModule } from '@angular/material/button';
import { MatTabsModule } from '@angular/material/tabs';
import { MatListModule } from '@angular/material/list';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MarchCalculatorThroughputTimeComponent } from './march-calculator/march-calculator-throughput-time/march-calculator-throughput-time.component';
import { MarchCalculatorMarchTimeComponent } from './march-calculator/march-calculator-march-time/march-calculator-march-time.component';
import { MarchCalculatorMarchColumnDistanceComponent } from './march-calculator/march-calculator-march-column-distance/march-calculator-march-column-distance.component';

const routes: Routes = [
  { path: 'march-calculator', component: MarchCalculatorComponent }
]

@NgModule({
  declarations: [
    AppComponent,
    MarchCalculatorComponent,
    MarchCalculatorLengthComponent,
    MarchCalculatorThroughputTimeComponent,
    MarchCalculatorMarchTimeComponent,
    MarchCalculatorMarchColumnDistanceComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatInputModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    RouterModule.forRoot(
      routes
    ),
    LayoutModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
