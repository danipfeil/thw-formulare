import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchCalculatorLengthComponent } from './march-calculator-length.component';

describe('MarchCalculatorLengthComponent', () => {
  let component: MarchCalculatorLengthComponent;
  let fixture: ComponentFixture<MarchCalculatorLengthComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarchCalculatorLengthComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchCalculatorLengthComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
