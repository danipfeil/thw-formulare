import { Component, OnChanges, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-march-calculator-length',
  templateUrl: './march-calculator-length.component.html',
  styleUrls: ['./march-calculator-length.component.scss']
})
export class MarchCalculatorLengthComponent implements OnInit {
  calculateByAmountOfVehiclesAndTrailersForm = new FormGroup({
    vehicleAmount: new FormControl(3),
    vehicleLength: new FormControl(7),
    trailerAmount: new FormControl(0),
    trailerLength: new FormControl(7),
    vehicleDistance: new FormControl(50),
    marchingColumnAmount: new FormControl(1),
    marchingColumnDistance: new FormControl(10)
  });
  result ='';

  calculate(): number{
    const controls = this.calculateByAmountOfVehiclesAndTrailersForm.controls

    console.log(this.calculateByAmountOfVehiclesAndTrailersForm.errors);

    return this.convertMeterToKilometer(
      (controls['vehicleLength'].value + controls['vehicleDistance'].value) * controls['vehicleAmount'].value 
        + controls['trailerAmount'].value * controls['trailerLength'].value
    ) + (controls['marchingColumnAmount'].value - 1) * controls['marchingColumnDistance'].value;
  }

  ngOnInit(): void {
    this.result = "Die berechnete Marschlänge beträgt: " + this.calculate().toString() + "km";

    this.calculateByAmountOfVehiclesAndTrailersForm.valueChanges.subscribe(() => {
      if(this.calculateByAmountOfVehiclesAndTrailersForm.valid){
        this.result = "Die berechnete Marschlänge beträgt: " + this.calculate().toString() + "km"
      } else {
        this.result = "Bitte füllen sie das Formular korrekt aus!"
      }
    })
  }

  private convertMeterToKilometer(meters: number): number{
    return meters/1000
  }
}
