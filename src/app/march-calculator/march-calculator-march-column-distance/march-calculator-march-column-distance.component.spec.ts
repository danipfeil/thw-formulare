import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchCalculatorMarchColumnDistanceComponent } from './march-calculator-march-column-distance.component';

describe('MarchCalculatorMarchColumnDistanceComponent', () => {
  let component: MarchCalculatorMarchColumnDistanceComponent;
  let fixture: ComponentFixture<MarchCalculatorMarchColumnDistanceComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarchCalculatorMarchColumnDistanceComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchCalculatorMarchColumnDistanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
