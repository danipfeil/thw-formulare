import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-march-calculator-march-column-distance',
  templateUrl: './march-calculator-march-column-distance.component.html',
  styleUrls: ['./march-calculator-march-column-distance.component.scss']
})
export class MarchCalculatorMarchColumnDistanceComponent implements OnInit {
  throughputTimeForm = new FormGroup({
    marchColumnDistance: new FormControl(1),
    marchSpeed: new FormControl(60)
  });
  result ='';

  calculate(): number{
    const controls = this.throughputTimeForm.controls

    return this.convertToMinute(controls['marchColumnDistance'].value * controls['marchSpeed'].value)
  }

  ngOnInit(): void {
    this.result = "Der berechnete Marschabstand beträgt: " + this.calculate().toString() + " Minuten";

    this.throughputTimeForm.valueChanges.subscribe(() => {
      if(this.throughputTimeForm.valid){
        this.result = "Der berechnete Marschabstand beträgt: " + this.calculate().toString() + " Minuten"
      } else {
        this.result = "Bitte füllen sie das Formular korrekt aus!"
      }
    })
  }

  convertToMinute(seconds: number): number {
    return seconds / 60;
  }
}
