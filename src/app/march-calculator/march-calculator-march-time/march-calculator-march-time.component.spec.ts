import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchCalculatorMarchTimeComponent } from './march-calculator-march-time.component';

describe('MarchCalculatorMarchTimeComponent', () => {
  let component: MarchCalculatorMarchTimeComponent;
  let fixture: ComponentFixture<MarchCalculatorMarchTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarchCalculatorMarchTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchCalculatorMarchTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
