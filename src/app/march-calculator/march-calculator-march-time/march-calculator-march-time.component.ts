import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-march-calculator-march-time',
  templateUrl: './march-calculator-march-time.component.html',
  styleUrls: ['./march-calculator-march-time.component.scss']
})
export class MarchCalculatorMarchTimeComponent implements OnInit {
  throughputTimeForm = new FormGroup({
    marchDistance: new FormControl(10),
    marchSpeed: new FormControl(60),
    marchPause: new FormControl(0),
    marchThroughputTime: new FormControl(0)
  });
  result = '';

  calculateFirstVehicle(): number{
    const controls = this.throughputTimeForm.controls

    return this.convertToMinute(controls['marchDistance'].value) / controls['marchSpeed'].value + controls['marchPause'].value
  }

  calculateLastVehicle(): number{
    return this.calculateFirstVehicle() + this.throughputTimeForm.controls['marchThroughputTime'].value
  }

  ngOnInit(): void {
    this.result = this.appendLastVehicleIfPossible("Die berechnete Marschzeit des ersten Fahrzeug beträgt: " + this.calculateFirstVehicle().toString() + " Minuten") 

    this.throughputTimeForm.valueChanges.subscribe(() => {
      if(this.throughputTimeForm.valid){
        this.result = this.appendLastVehicleIfPossible("Die berechnete Marschzeit des ersten Fahrzeug beträgt: " + this.calculateFirstVehicle().toString() + " Minuten")
      } else {
        this.result = "Bitte füllen sie das Formular korrekt aus!"
      }
    })
  }

  private appendLastVehicleIfPossible(currentResult: string): string {
    if(this.throughputTimeForm.controls['marchThroughputTime'].value > 0){
      return currentResult + ", die des letzten Fahrzeug: " + this.calculateLastVehicle() + " Minuten"
    }
    return currentResult;
  }

  private convertToMinute(hours: number): number {
    return hours * 60;
  }
}

