import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchCalculatorThroughputTimeComponent } from './march-calculator-throughput-time.component';

describe('MarchCalculatorThroughputTimeComponent', () => {
  let component: MarchCalculatorThroughputTimeComponent;
  let fixture: ComponentFixture<MarchCalculatorThroughputTimeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarchCalculatorThroughputTimeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchCalculatorThroughputTimeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
