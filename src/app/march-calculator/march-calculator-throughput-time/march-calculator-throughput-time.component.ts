import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-march-calculator-throughput-time',
  templateUrl: './march-calculator-throughput-time.component.html',
  styleUrls: ['./march-calculator-throughput-time.component.scss']
})
export class MarchCalculatorThroughputTimeComponent implements OnInit {
  throughputTimeForm = new FormGroup({
    marchLength: new FormControl(0.5),
    marchSpeed: new FormControl(60)
  });
  result ='';

  calculate(): number{
    const controls = this.throughputTimeForm.controls

    return this.convertToMinute(controls['marchLength'].value / controls['marchSpeed'].value)
  }

  ngOnInit(): void {
    this.result = "Die berechnete Durchlaufzeit beträgt: " + this.calculate().toString() + " Minuten";

    this.throughputTimeForm.valueChanges.subscribe(() => {
      if(this.throughputTimeForm.valid){
        this.result = "Die berechnete Durchlaufzeit beträgt: " + this.calculate().toString() + " Minuten"
      } else {
        this.result = "Bitte füllen sie das Formular korrekt aus!"
      }
    })
  }

  convertToMinute(hours: number): number {
    return hours * 60;
  }
}


