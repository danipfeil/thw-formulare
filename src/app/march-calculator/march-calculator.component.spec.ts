import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarchCalculatorComponent } from './march-calculator.component';

describe('MarchCalculatorComponent', () => {
  let component: MarchCalculatorComponent;
  let fixture: ComponentFixture<MarchCalculatorComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarchCalculatorComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarchCalculatorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
